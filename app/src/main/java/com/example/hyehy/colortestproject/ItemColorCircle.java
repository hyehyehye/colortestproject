package com.example.hyehy.colortestproject;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemColorCircle implements Parcelable {
    int colorLeft;
    int colorRight;

    String result;

    public ItemColorCircle() {
    }

    public ItemColorCircle(Parcel in) {
        colorLeft = in.readInt();
        colorRight = in.readInt();
        result = in.readString();
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(colorLeft);
        parcel.writeInt(colorRight);
        parcel.writeString(result);
    }

    public static final Parcelable.Creator<ItemColorCircle> CREATOR = new Parcelable.Creator<ItemColorCircle>() {
        public ItemColorCircle createFromParcel(Parcel in) {
            return new ItemColorCircle(in);
        }

        public ItemColorCircle[] newArray(int size) {
            return new ItemColorCircle[size];
        }
    };
}
