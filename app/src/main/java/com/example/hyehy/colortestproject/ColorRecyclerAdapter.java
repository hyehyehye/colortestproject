package com.example.hyehy.colortestproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;


public class ColorRecyclerAdapter extends RecyclerView.Adapter<ColorViewholder>{
    ArrayList<ItemColorCircle> mList = new ArrayList<ItemColorCircle>();
    Context mContext;

    public ColorRecyclerAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public ColorViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        ConstraintLayout v = (ConstraintLayout) ConstraintLayout.inflate(mContext, R.layout.item_color_circle, null);
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(Constant.SELECTED_CIRCLE_SIZE, Constant.SELECTED_CIRCLE_SIZE);
        lp.setMargins(GlobalUtils.dpToPx(10), GlobalUtils.dpToPx(10), GlobalUtils.dpToPx(10), GlobalUtils.dpToPx(10));
        v.setLayoutParams(lp);
        v.setBackground(mContext.getDrawable(R.drawable.bg_circle));
        v.setClipToOutline(true);

        return new ColorViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorViewholder colorCircleViewholder, int position) {
        colorCircleViewholder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    ArrayList<ItemColorCircle> getSelectedList() {
        return mList;
    }

    public void select(ItemColorCircle item) {
        if (isExist(item)) {
            mList.remove(item);
            notifyDataSetChanged();
            return;
        }

        int itemNum1 = mList.size();
        if (itemNum1 >= 4) {
            Toast.makeText(mContext, "색상은 4개까지만 선택가능합니다:)",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        mList.add(item);
        notifyDataSetChanged();
    }

    private boolean isExist(ItemColorCircle target) {
        for (ItemColorCircle item : mList) {
            if (item.equals(target)) return true;
        }
        return false;
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

}
