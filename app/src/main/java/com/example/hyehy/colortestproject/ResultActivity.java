package com.example.hyehy.colortestproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;


public class ResultActivity  extends AppCompatActivity {

    RecyclerView rvResult;
    ResultRecyclerAdapter mAdapter;
    LinearLayout containerPagination;
    LinearLayoutManager lm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        setButton();

        rvResult = findViewById(R.id.rv_result);
        mAdapter = new ResultRecyclerAdapter(this);
        rvResult.setAdapter(mAdapter);
        lm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvResult.setLayoutManager(lm);

        /* SnapHelper : 아이템이 중앙에 위치되게 정렬*/
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(rvResult);


        ArrayList<ItemColorCircle> items = getIntent().getParcelableArrayListExtra("selectedItem");
        setPagination(items);
        for (ItemColorCircle item : items) { mAdapter.add(item); }

        //우측상단의 하찮은 버튼
        Button btnNote = findViewById(R.id.btn_note);
        btnNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, NoteActivity.class);
                startActivity(intent);
            }
        });

    }

    void setButton() {
        Button btn1 = findViewById(R.id.btn_again);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.end_enter, R.anim.end_exit);
            }
        });
    }

    void setPagination(final List<ItemColorCircle> list) {
        containerPagination = findViewById(R.id.container_pagination);
        drawPagination(list, 0);

        /* 리사이클러뷰 스크롤 리스너 */
        rvResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    drawPagination(list, lm.findFirstVisibleItemPosition());
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    void drawPagination(List<ItemColorCircle> list, int curPosition) {
        if (containerPagination.getChildCount() != 0) {
            containerPagination.removeAllViews();
        }

        int size = list.size();
        for (int i = 0 ; i < size ; i++ ) {
            View v = View.inflate(this, R.layout.item_color_circle, null);
            v.setClipToOutline(true);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(GlobalUtils.dpToPx(10), GlobalUtils.dpToPx(10));
            if (i != size -1) { lp.rightMargin = GlobalUtils.dpToPx(4); }
            v.setLayoutParams(lp);

            if (i == curPosition) {
                v.setBackgroundTintList(getColorStateList(R.color.colorAccent));
            }else{
                v.setBackgroundTintList(getColorStateList(R.color.white));
            }

            containerPagination.addView(v);
        }
    }
}
