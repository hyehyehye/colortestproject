package com.example.hyehy.colortestproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class NoteActivity  extends AppCompatActivity {
    TextView btnBack;
    TextView btnOpensource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnOpensource = findViewById(R.id.note_btn1);
        btnOpensource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(NoteActivity.this, "╯°□°)╯︵ ʞɹoʍ", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

