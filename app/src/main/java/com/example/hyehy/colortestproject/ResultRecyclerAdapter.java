package com.example.hyehy.colortestproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;


public class ResultRecyclerAdapter extends RecyclerView.Adapter<ResultViewHolder>{
    ArrayList<ItemColorCircle> mList = new ArrayList<ItemColorCircle>();
    Context mContext;

    public ResultRecyclerAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        CardView v = (CardView) CardView.inflate(mContext, R.layout.item_result_card, null);

        int widthPixels = mContext.getResources().getDisplayMetrics().widthPixels;
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(widthPixels - GlobalUtils.dpToPx(40), GlobalUtils.dpToPx(400));
        lp.setMargins(GlobalUtils.dpToPx(20),GlobalUtils.dpToPx(30),GlobalUtils.dpToPx(20),GlobalUtils.dpToPx(30));
        v.setLayoutParams(lp);

        return new ResultViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder resultViewholder, int position) {
        resultViewholder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() { return mList.size(); }

    public void add(ItemColorCircle item) {
        mList.add(item);
        notifyDataSetChanged();
    }



}
