package com.example.hyehy.colortestproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nex3z.flowlayout.FlowLayout;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ColorRecyclerAdapter mAdapter;

    FlowLayout mainCircleContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycleView1);
        mainCircleContainer = findViewById(R.id.container_circles);

        mAdapter = new ColorRecyclerAdapter(this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        initCircles();

        // 결과보기 누르면 일어나는 일
        // if itemNum==0 이면 선택하라고 토스트, 아니면 intent로 넘어감
        //이거도 사실 왜 되는지 모르겟음ㅎ 맞음?ㅎㅎ
        // => 네네 맞습니다!
        // => 위에 정확하게 로직 설명하고 계시면서 모른다니..
        // => 추가로, else를 빼고 return을 넣으면 좀 더 깔끔해집니다~~
        Button btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               int itemNum = mAdapter.getItemCount();
               if (itemNum == 0) {
                   Toast.makeText(MainActivity.this, R.string.txt_no_circle_selected, Toast.LENGTH_SHORT).show();
                   return;
               }

               Intent intent = new Intent(MainActivity.this, ResultActivity.class);
               intent.putParcelableArrayListExtra("selectedItem", mAdapter.getSelectedList());
               startActivity(intent);
               overridePendingTransition(R.anim.start_enter, R.anim.start_exit);
           }
        });

    }

    @Override
    public void onResume(){
        super.onResume();

        mAdapter.clear();
    }

    //원 36개 만드는거
    void initCircles() {
        for (int i = 0; i < 36; i++) {
            final ItemColorCircle item = new ItemColorCircle();

            Pair<Integer, Integer> color = getCircleColor(i);
            item.colorLeft = color.first;
            item.colorRight = color.second;
            item.result = getCircleResult(i);


            ConstraintLayout view = (ConstraintLayout) ConstraintLayout.inflate(this, R.layout.item_color_circle, null);
            view.setLayoutParams(new ConstraintLayout.LayoutParams(Constant.NORMAL_CIRCLE_SIZE, Constant.NORMAL_CIRCLE_SIZE));
            view.setBackground(getDrawable(R.drawable.bg_circle));
            view.setClipToOutline(true);

            ColorViewholder vh = new ColorViewholder(view);
            vh.viewLeft.setBackgroundColor(item.colorLeft);
            vh.viewRight.setBackgroundColor(item.colorRight);

            //원이 눌렸을때 반응
            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAdapter.select(item);
                }
            });

            mainCircleContainer.addView(view);
        }
    }

    /**
     * 동그란 친구의 색상을 결정하는 함수.
     * position에 따라 다른 색상값을 적용함.
     *
     * @param position 위치값
     */
    Pair<Integer, Integer> getCircleColor(int position) {
        int leftColor;
        int rightColor;

        switch (position) {
            case 0:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_1);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 1:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_2);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_13);
                break;

            case 2:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_3);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_3);
                break;

            case 3:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_4);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_5);
                break;

            case 4:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_9);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_7);
                break;

            case 5:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_6);
                break;

            case 6:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_3);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_2);
                break;

            case 7:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_7);
                break;

            case 8:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_13);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_8);
                break;

            case 9:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_9);
                break;

            case 10:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_3);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 11:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_7);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 12:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 13:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_9);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_9);
                break;

            case 14:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_3);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 15:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_7);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_7);
                break;

            case 16:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_8);
                break;

            case 17:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_9);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_13);
                break;

            case 18:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_13);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_7);
                break;

            case 19:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_8);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_2);
                break;

            case 20:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_13);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_13);
                break;

            case 21:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_9);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_2);
                break;

            case 22:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_13);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 23:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_7);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_2);
                break;

            case 24:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_1);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_2);
                break;

            case 25:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_10);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_10);
                break;

            case 26:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_13);
                break;

            case 27:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_8);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_1);
                break;

            case 28:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_11);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_11);
                break;

            case 29:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_3);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_2);
                break;

            case 30:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_8);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_8);
                break;

            case 31:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_6);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_3);
                break;

            case 32:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_12);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_7);
                break;

            case 33:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_13);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_3);
                break;

            case 34:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_2);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_6);
                break;

            case 35:
                leftColor = ContextCompat.getColor(this, R.color.sample_color_12);
                rightColor = ContextCompat.getColor(this, R.color.sample_color_12);
                break;


            default:
                leftColor = ContextCompat.getColor(this, R.color.black);
                rightColor = ContextCompat.getColor(this, R.color.black);
                break;
        }

        return new Pair<>(leftColor, rightColor);
    }

    //결과 36개
    String getCircleResult(int position) {

        String result = "";
        switch (position) {
            case 0:
                result = getString(R.string.test_result_1);
                break;

            case 1:
                result = getString(R.string.test_result_2);
                break;

            case 2:
                result = getString(R.string.test_result_3);
                break;

            case 3:
                result = getString(R.string.test_result_4);
                break;

            case 4:
                result = getString(R.string.test_result_5);
                break;

            case 5:
                result = getString(R.string.test_result_6);
                break;

            case 6:
                result = getString(R.string.test_result_7);
                break;

            case 7:
                result = getString(R.string.test_result_8);
                break;

            case 8:
                result = getString(R.string.test_result_9);
                break;

            case 9:
                result = getString(R.string.test_result_10);
                break;

                case 10:
                result = getString(R.string.test_result_11);
                break;

            case 11:
                result = getString(R.string.test_result_12);
                break;

            case 12:
                result = getString(R.string.test_result_13);
                break;

            case 13:
                result = getString(R.string.test_result_14);
                break;

            case 14:
                result = getString(R.string.test_result_15);
                break;

            case 15:
                result = getString(R.string.test_result_16);
                break;

            case 16:
                result = getString(R.string.test_result_17);
                break;

            case 17:
                result = getString(R.string.test_result_18);
                break;

            case 18:
                result = getString(R.string.test_result_19);
                break;

            case 19:
                result = getString(R.string.test_result_20);
                break;

            case 20:
                result = getString(R.string.test_result_21);
                break;

            case 21:
                result = getString(R.string.test_result_22);
                break;

            case 22:
                result = getString(R.string.test_result_23);
                break;

            case 23:
                result = getString(R.string.test_result_24);
                break;

            case 24:
                result = getString(R.string.test_result_25);
                break;

            case 25:
                result = getString(R.string.test_result_26);
                break;

            case 26:
                result = getString(R.string.test_result_27);
                break;

            case 27:
                result = getString(R.string.test_result_28);
                break;

            case 28:
                result = getString(R.string.test_result_29);
                break;

            case 29:
                result = getString(R.string.test_result_30);
                break;

            case 30:
                result = getString(R.string.test_result_31);
                break;

            case 31:
                result = getString(R.string.test_result_32);
                break;

            case 32:
                result = getString(R.string.test_result_33);
                break;

            case 33:
                result = getString(R.string.test_result_34);
                break;

            case 34:
                result = getString(R.string.test_result_35);
                break;

            case 35:
                result = getString(R.string.test_result_36);
                break;

            default:
                result = "";
                break;
        }

        return result;
    }
}
