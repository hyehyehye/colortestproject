package com.example.hyehy.colortestproject;


import android.support.v7.widget.RecyclerView;
import android.view.View;


//리사이클러뷰 쓰려면 ctrl shift alt s  -> dependences 에서 추가해줘야함
public class ColorViewholder extends RecyclerView.ViewHolder{
    View viewLeft;
    View viewRight;

    public void bind(ItemColorCircle circle) {

        int leftColor = circle.colorLeft;
        int rightColor = circle.colorRight;

        viewLeft.setBackgroundColor(leftColor);
        viewRight.setBackgroundColor(rightColor);
    }

    public ColorViewholder(View view) {
        super(view);
        viewLeft = view.findViewById(R.id.viewLeft);
        viewRight = view.findViewById(R.id.viewRight);
    }
}
