package com.example.hyehy.colortestproject;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by minseok on 2018. 7. 27..
 * colortestproject.
 */
public class GlobalUtils {

    static int dpToPx(int dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }
}
