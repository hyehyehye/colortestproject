package com.example.hyehy.colortestproject;

/**
 * Created by minseok on 2018. 7. 27..
 * colortestproject.
 */
public class Constant {
    static int NORMAL_CIRCLE_SIZE = GlobalUtils.dpToPx(45);
    static int SELECTED_CIRCLE_SIZE = GlobalUtils.dpToPx(40);
}
