package com.example.hyehy.colortestproject;

import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;


public class ResultViewHolder extends RecyclerView.ViewHolder {
    View containerCircle;
    View leftInCircle;
    View rightInCircle;
    TextView txtResult;

    public void bind(ItemColorCircle circle) {
        containerCircle.setClipToOutline(true);

        leftInCircle.setBackgroundColor(circle.colorLeft);
        rightInCircle.setBackgroundColor(circle.colorRight);

        txtResult.setText(circle.result);
        txtResult.setMovementMethod(new ScrollingMovementMethod());
    }

    public ResultViewHolder(View view) {
        super(view);

        containerCircle = view.findViewById(R.id.item_circle);
        leftInCircle = view.findViewById(R.id.item_left);
        rightInCircle = view.findViewById(R.id.item_right);
        txtResult = view.findViewById(R.id.txt_result);
    }


}
